(function() {
	var module = angular.module('pomodoroapp');
	module.service('teamService', [ '$http', '$q', function($http, $q) {

		var service = this;

		service.getAllTeams = function() {
			var deferred = $q.defer();
			$http.get('allTeams').then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}

	} ]);
})();