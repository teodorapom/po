(function() {
	var module = angular.module('pomodoroapp');
	module.controller('TeamController', [ '$scope', 'teamService',
			function($scope, teamService) {

				$scope.getAllTeams = function() {
					teamService.getAllTeams().then(function(response) {
						$scope.allTeams = response.data;
					}, function(error) {
						console.log(error);
					});
				}

				var initialization = function() {
					$scope.getAllTeams();
				}
				initialization();

			} ]);
})();
